/*
 * Copyright 2018 kaszaq.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package pl.kaszaq.howfastyouaregoing;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author kaszaq
 */
public class J2M {

    private static String replaceTables(String text) {
        Pattern p = Pattern.compile("^((\\|\\|[^\\|]+?)+)\\|\\|$", Pattern.MULTILINE);
        Matcher m = p.matcher(text);
        StringBuffer sb = new StringBuffer();
        while (m.find()) {
            String header = m.group(0);
            int howManyColumns = StringUtils.countMatches(header, "||") - 1;
            header = header.replaceAll("\\|\\|", "|");
            header = "\n" + header + "\n" + StringUtils.repeat("| --- ", howManyColumns) + "|";
            m.appendReplacement(sb, header);
        }
        m.appendTail(sb);
        return sb.toString();
    }

    // Based on https://github.com/FokkeZB/J2M/blob/master/src/J2M.js
    static String changeToMarkdown(String jiraText) {
        if (jiraText == null) {
            return "";
        }
        // .bq - quote
        jiraText = replace(jiraText, "^bq\\.(.*)$", "> $1\n");
        // bold and italic
        jiraText = replace(jiraText, "([*_])(.*)\\1", (Matcher m) -> {
            String to = m.group(1).charAt(0) == '*' ? "**" : "*";
            return to + "$2" + to;
        });
        // lists
        jiraText = replace(jiraText, "^((?:#|-|\\+|\\*)+) (.*)$", (Matcher m) -> {
            String level = m.group(1);
            String prefix = level.substring(level.length() - 1);
            if (prefix.equals("#")) {
                prefix = "1.";
            }
            StringBuilder response = new StringBuilder("\n");
            for (int i = 0; i < (level.length() - 1) * 4 + 2; i++) {
                response.append(" ");
            }
            response.append(prefix).append(" ").append("$2");
            return response.toString();
        });
        // headers, must be after numbered lists
        jiraText = replace(jiraText, "^h([0-6])\\.(.*)$", (Matcher m) -> {
            String level = m.group(1);
            StringBuilder response = new StringBuilder();
            for (int i = 0; i < (Integer.parseInt(level)); i++) {
                response.append("#");
            }
            response.append("$2");
            return response.toString();
        });
        // monospaced
        jiraText = replace(jiraText, "\\{\\{([^}]+)\\}\\}", "`$1`");
        // citation
        jiraText = replace(jiraText, "\\?\\?((?:.[^?]|[^?].)+)\\?\\?", "<cite>$1</cite>");
        // inserted
        jiraText = replace(jiraText, "\\+([^+]*)\\+", "<ins>$1</ins>");
        // superscript
        jiraText = replace(jiraText, "\\^([^^]*)\\^", "<sup>$1</sup>");
        // subscript
        jiraText = replace(jiraText, "~([^~]*)~", "<sub>$1</sub>");
        // deleted [dont know why, seem to replace to the same thing?] todo: delete?
        jiraText = replace(jiraText, "(\\s)-([\\w\\d].*?[\\w\\d]+)-(\\s)", "$1~~$2~~$3");
        // code
        jiraText = replace(jiraText, "\\{code(:([a-z]+))?\\}(.*?)\\{code\\}", "```$2$3```", Pattern.DOTALL | Pattern.MULTILINE);
        //quote
        jiraText = replace(jiraText, "\\{quote\\}(.*?)\\{quote\\}", (Matcher m) -> {
            String content = m.group(1);
            String[] lines = content.split("\\r?\\n");
            StringBuilder sb = new StringBuilder();
            boolean first = true;
            for (String line : lines) {
                if (first && line.isEmpty()) {
                    continue;
                }
                sb.append("> ").append(line).append("\n");
            }
            sb.append("\n");
            return Matcher.quoteReplacement(sb.toString());
        }, Pattern.DOTALL | Pattern.MULTILINE);
        // pictures
        jiraText = replace(jiraText, "!([^\\n\\s]+)!", "![]($1)");
        // urls with text
        jiraText = replace(jiraText, "\\[([^|]+)\\|(.+?)\\]", "[$1]($2)");
        //urls without text
        jiraText = replace(jiraText, "\\[(.+?)\\]([^\\(]+)", "<$1>$2");
        // noformat
        jiraText = replace(jiraText, "\\{noformat\\}", "```", 0);
        // color
        jiraText = replace(jiraText, "\\{color:([^}]+)\\}(.*?)\\{color\\}", "<span style=\"color:$1\">$2</span>", Pattern.DOTALL | Pattern.MULTILINE);
        //table
        jiraText = replaceTables(jiraText);
        return jiraText;
    }

    private static String replace(String text, String from, String to) {
        return replace(text, from, to, Pattern.MULTILINE);
    }

    private static String replace(String text, String from, String to, int flags) {
        return replace(text, from, (Matcher m) -> to, flags);
    }

    private static String replace(String text, String from, RegexToFactory to) {
        return replace(text, from, to, Pattern.MULTILINE);
    }

    private static String replace(String text, String from, RegexToFactory to, int flags) {
        Pattern p = Pattern.compile(from, flags);
        Matcher m = p.matcher(text);
        StringBuffer sb = new StringBuffer(text.length());
        while (m.find()) {
            m.appendReplacement(sb, to.createReplacement(m));
        }
        m.appendTail(sb);
        return sb.toString();
    }

    @FunctionalInterface
    private static interface RegexToFactory {

        String createReplacement(Matcher m);
    }
}
