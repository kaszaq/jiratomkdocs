/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.kaszaq.howfastyouaregoing;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author kaszaq
 */
public class MainTest {

    @Test
    public void testMarkdownConversion1() {
        String given = "bq. Some block quoted text\n"
                + "*zonk* ale nie wazne gdy _cos_ sie w koncu dzieje? :)\n"
                + "# asd\n"
                + "# asd\n"
                + "# asd\n"
                + "## asd\n"
                + "#### 3333\n"
                + "#- asd\n"
                + "h1. Biggest heading\n"
                + "				  \n"
                + "h2. Bigger heading\n"
                + "\n"
                + "h1. Biggest heading\n"
                + "h2. Bigger heading\n"
                + "h3. Big heading\n"
                + "h4. Normal heading\n"
                + "h5. Small heading\n"
                + "h6. Smallest heading\n"
                + "\n"
                + "*strong*\n"
                + "_emphasis_\n"
                + "{{monospaced}}\n"
                + "??citation??\n"
                + "-deleted-\n"
                + "+inserted+\n"
                + "^superscript^\n"
                + "~subscript~\n"
                + "\n"
                + "{code:javascript}\n"
                + "var hello = 'world';\n"
                + "{code}\n"
                + "\n"
                + "{quote}\n"
                + "some thin really strange mon\n"
                + "{quote}\n"
                + "\n"
                + "[http://google.com]\n"
                + "[Google|http://google.com]\n"
                + "{quote}\n"
                + "another quote\n"
                + "{quote}\n"
                + "!http://www.host.com/image.gif!\n"
                + "or\n"
                + "!attached-image.gif!\n"
                + "\n"
                + "{noformat}\n"
                + "preformatted piece of text\n"
                + " so *no* further _formatting_ is done here\n"
                + "{noformat}\n"
                + "{color:red}\n"
                + "    look ma, red text!\n"
                + "{color}"
                + "\n"
                + "||heading 1||heading 2||heading 3||\n"
                + "|col A1|col A2|col A3|\n"
                + "|col B1|col B2|col B3|\n"
                + "\n"
                + "||heading 1||heading 2||heading 3||heading 3||\n"
                + "|col A1|col A2|col A3|col A3|\n"
                + "|col B1|col B2|col B3|col A3|";
        String expected = ">  Some block quoted text\n\n"
                + "**zonk** ale nie wazne gdy *cos* sie w koncu dzieje? :)\n"
                + "\n"
                + "  1. asd\n"
                + "\n"
                + "  1. asd\n"
                + "\n"
                + "  1. asd\n"
                + "\n"
                + "      1. asd\n"
                + "\n"
                + "              1. 3333\n"
                + "\n"
                + "      - asd\n"
                + "# Biggest heading\n"
                + "				  \n"
                + "## Bigger heading\n"
                + "\n"
                + "# Biggest heading\n"
                + "## Bigger heading\n"
                + "### Big heading\n"
                + "#### Normal heading\n"
                + "##### Small heading\n"
                + "###### Smallest heading\n"
                + "\n"
                + "**strong**\n"
                + "*emphasis*\n"
                + "`monospaced`\n"
                + "<cite>citation</cite>\n"
                + "~~deleted~~\n"
                + "<ins>inserted</ins>\n"
                + "<sup>superscript</sup>\n"
                + "<sub>subscript</sub>\n"
                + "\n"
                + "```javascript\n"
                + "var hello = 'world';\n"
                + "```\n"
                + "\n"
                + "> some thin really strange mon\n"
                + "\n"
                + "\n"
                + "\n"
                + "<http://google.com>\n"
                + "[Google](http://google.com)\n"
                + "> another quote\n"
                + "\n"
                + "\n"
                + "![](http://www.host.com/image.gif)\n"
                + "or\n"
                + "![](attached-image.gif)\n"
                + "\n"
                + "```\n"
                + "preformatted piece of text\n"
                + " so **no** further *formatting* is done here\n"
                + "```\n"
                + "<span style=\"color:red\">\n"
                + "    look ma, red text!\n"
                + "</span>"
                + "\n"
                + "\n|heading 1|heading 2|heading 3|\n"
                + "| --- | --- | --- |\n"
                + "|col A1|col A2|col A3|\n"
                + "|col B1|col B2|col B3|\n"
                + "\n"
                + "\n|heading 1|heading 2|heading 3|heading 3|\n"
                + "| --- | --- | --- | --- |\n"
                + "|col A1|col A2|col A3|col A3|\n"
                + "|col B1|col B2|col B3|col A3|";
        final String result = J2M.changeToMarkdown(given);
        System.out.println(result);
        System.out.println("----");
        assertEquals(expected, result);
    }


}
